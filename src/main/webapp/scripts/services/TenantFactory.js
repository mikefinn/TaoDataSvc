angular.module('taoDataSvc').factory('TenantResource', function($resource){
    var resource = $resource('rest/tenants/:TenantId',{TenantId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});