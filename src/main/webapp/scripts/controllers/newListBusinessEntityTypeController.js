
angular.module('taoDataSvc').controller('NewListBusinessEntityTypeController', function ($scope, $location, locationParser, flash, ListBusinessEntityTypeResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.listBusinessEntityType = $scope.listBusinessEntityType || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The listBusinessEntityType was created successfully.'});
            $location.path('/ListBusinessEntityTypes');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        ListBusinessEntityTypeResource.save($scope.listBusinessEntityType, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/ListBusinessEntityTypes");
    };
});