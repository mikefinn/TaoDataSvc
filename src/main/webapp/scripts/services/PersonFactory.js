angular.module('taoDataSvc').factory('PersonResource', function($resource){
    var resource = $resource('rest/people/:PersonId',{PersonId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});