
angular.module('taoDataSvc').controller('NewContactAddressController', function ($scope, $location, locationParser, flash, ContactAddressResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.contactAddress = $scope.contactAddress || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The contactAddress was created successfully.'});
            $location.path('/ContactAddresses');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        ContactAddressResource.save($scope.contactAddress, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/ContactAddresses");
    };
});