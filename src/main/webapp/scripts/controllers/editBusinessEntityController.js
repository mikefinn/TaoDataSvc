

angular.module('taoDataSvc').controller('EditBusinessEntityController', function($scope, $routeParams, $location, flash, BusinessEntityResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.businessEntity = new BusinessEntityResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The businessEntity could not be found.'});
            $location.path("/BusinessEntities");
        };
        BusinessEntityResource.get({BusinessEntityId:$routeParams.BusinessEntityId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.businessEntity);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The businessEntity was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.businessEntity.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/BusinessEntities");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The businessEntity was deleted.'});
            $location.path("/BusinessEntities");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.businessEntity.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});