angular.module('taoDataSvc').factory('ListBusinessEntityTypeResource', function($resource){
    var resource = $resource('rest/listbusinessentitytypes/:ListBusinessEntityTypeId',{ListBusinessEntityTypeId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});