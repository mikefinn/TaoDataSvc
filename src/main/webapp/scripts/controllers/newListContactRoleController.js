
angular.module('taoDataSvc').controller('NewListContactRoleController', function ($scope, $location, locationParser, flash, ListContactRoleResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.listContactRole = $scope.listContactRole || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The listContactRole was created successfully.'});
            $location.path('/ListContactRoles');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        ListContactRoleResource.save($scope.listContactRole, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/ListContactRoles");
    };
});