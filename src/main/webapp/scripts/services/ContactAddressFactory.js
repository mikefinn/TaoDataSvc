angular.module('taoDataSvc').factory('ContactAddressResource', function($resource){
    var resource = $resource('rest/contactaddresses/:ContactAddressId',{ContactAddressId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});