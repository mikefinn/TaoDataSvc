
angular.module('taoDataSvc').controller('NewListPhoneWirelessCarrierController', function ($scope, $location, locationParser, flash, ListPhoneWirelessCarrierResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.listPhoneWirelessCarrier = $scope.listPhoneWirelessCarrier || {};
    
    $scope.supportsSmsList = [
        "true",
        "false"
    ];

    $scope.isSMSEnabledList = [
        "true",
        "false"
    ];

    $scope.isMMSEnabledList = [
        "true",
        "false"
    ];


    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The listPhoneWirelessCarrier was created successfully.'});
            $location.path('/ListPhoneWirelessCarriers');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        ListPhoneWirelessCarrierResource.save($scope.listPhoneWirelessCarrier, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/ListPhoneWirelessCarriers");
    };
});