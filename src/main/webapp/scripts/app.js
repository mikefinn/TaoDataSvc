'use strict';

angular.module('taoDataSvc',['ngRoute','ngResource'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/',{templateUrl:'views/landing.html',controller:'LandingPageController'})
      .when('/BusinessEntities',{templateUrl:'views/BusinessEntity/search.html',controller:'SearchBusinessEntityController'})
      .when('/BusinessEntities/new',{templateUrl:'views/BusinessEntity/detail.html',controller:'NewBusinessEntityController'})
      .when('/BusinessEntities/edit/:BusinessEntityId',{templateUrl:'views/BusinessEntity/detail.html',controller:'EditBusinessEntityController'})
      .when('/ContactAddresses',{templateUrl:'views/ContactAddress/search.html',controller:'SearchContactAddressController'})
      .when('/ContactAddresses/new',{templateUrl:'views/ContactAddress/detail.html',controller:'NewContactAddressController'})
      .when('/ContactAddresses/edit/:ContactAddressId',{templateUrl:'views/ContactAddress/detail.html',controller:'EditContactAddressController'})
      .when('/ContactEmails',{templateUrl:'views/ContactEmail/search.html',controller:'SearchContactEmailController'})
      .when('/ContactEmails/new',{templateUrl:'views/ContactEmail/detail.html',controller:'NewContactEmailController'})
      .when('/ContactEmails/edit/:ContactEmailId',{templateUrl:'views/ContactEmail/detail.html',controller:'EditContactEmailController'})
      .when('/ContactPhones',{templateUrl:'views/ContactPhone/search.html',controller:'SearchContactPhoneController'})
      .when('/ContactPhones/new',{templateUrl:'views/ContactPhone/detail.html',controller:'NewContactPhoneController'})
      .when('/ContactPhones/edit/:ContactPhoneId',{templateUrl:'views/ContactPhone/detail.html',controller:'EditContactPhoneController'})
      .when('/ListBusinessEntityTypes',{templateUrl:'views/ListBusinessEntityType/search.html',controller:'SearchListBusinessEntityTypeController'})
      .when('/ListBusinessEntityTypes/new',{templateUrl:'views/ListBusinessEntityType/detail.html',controller:'NewListBusinessEntityTypeController'})
      .when('/ListBusinessEntityTypes/edit/:ListBusinessEntityTypeId',{templateUrl:'views/ListBusinessEntityType/detail.html',controller:'EditListBusinessEntityTypeController'})
      .when('/ListContactRoles',{templateUrl:'views/ListContactRole/search.html',controller:'SearchListContactRoleController'})
      .when('/ListContactRoles/new',{templateUrl:'views/ListContactRole/detail.html',controller:'NewListContactRoleController'})
      .when('/ListContactRoles/edit/:ListContactRoleId',{templateUrl:'views/ListContactRole/detail.html',controller:'EditListContactRoleController'})
      .when('/ListPersonRoles',{templateUrl:'views/ListPersonRole/search.html',controller:'SearchListPersonRoleController'})
      .when('/ListPersonRoles/new',{templateUrl:'views/ListPersonRole/detail.html',controller:'NewListPersonRoleController'})
      .when('/ListPersonRoles/edit/:ListPersonRoleId',{templateUrl:'views/ListPersonRole/detail.html',controller:'EditListPersonRoleController'})
      .when('/ListPhoneWirelessCarriers',{templateUrl:'views/ListPhoneWirelessCarrier/search.html',controller:'SearchListPhoneWirelessCarrierController'})
      .when('/ListPhoneWirelessCarriers/new',{templateUrl:'views/ListPhoneWirelessCarrier/detail.html',controller:'NewListPhoneWirelessCarrierController'})
      .when('/ListPhoneWirelessCarriers/edit/:ListPhoneWirelessCarrierId',{templateUrl:'views/ListPhoneWirelessCarrier/detail.html',controller:'EditListPhoneWirelessCarrierController'})
      .when('/Locations',{templateUrl:'views/Location/search.html',controller:'SearchLocationController'})
      .when('/Locations/new',{templateUrl:'views/Location/detail.html',controller:'NewLocationController'})
      .when('/Locations/edit/:LocationId',{templateUrl:'views/Location/detail.html',controller:'EditLocationController'})
      .when('/People',{templateUrl:'views/Person/search.html',controller:'SearchPersonController'})
      .when('/People/new',{templateUrl:'views/Person/detail.html',controller:'NewPersonController'})
      .when('/People/edit/:PersonId',{templateUrl:'views/Person/detail.html',controller:'EditPersonController'})
      .when('/Tenants',{templateUrl:'views/Tenant/search.html',controller:'SearchTenantController'})
      .when('/Tenants/new',{templateUrl:'views/Tenant/detail.html',controller:'NewTenantController'})
      .when('/Tenants/edit/:TenantId',{templateUrl:'views/Tenant/detail.html',controller:'EditTenantController'})
      .otherwise({
        redirectTo: '/'
      });
  }])
  .controller('LandingPageController', function LandingPageController() {
  })
  .controller('NavController', function NavController($scope, $location) {
    $scope.matchesRoute = function(route) {
        var path = $location.path();
        return (path === ("/" + route) || path.indexOf("/" + route + "/") == 0);
    };
  });
