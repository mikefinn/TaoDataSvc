

angular.module('taoDataSvc').controller('EditContactPhoneController', function($scope, $routeParams, $location, flash, ContactPhoneResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.contactPhone = new ContactPhoneResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The contactPhone could not be found.'});
            $location.path("/ContactPhones");
        };
        ContactPhoneResource.get({ContactPhoneId:$routeParams.ContactPhoneId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.contactPhone);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The contactPhone was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.contactPhone.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/ContactPhones");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The contactPhone was deleted.'});
            $location.path("/ContactPhones");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.contactPhone.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});