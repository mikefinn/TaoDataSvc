angular.module('taoDataSvc').factory('ListContactRoleResource', function($resource){
    var resource = $resource('rest/listcontactroles/:ListContactRoleId',{ListContactRoleId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});