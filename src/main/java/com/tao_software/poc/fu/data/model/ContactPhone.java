package com.tao_software.poc.fu.data.model;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Version;
import com.tao_software.poc.fu.data.model.ListContactRole;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class ContactPhone implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private static final long serialVersionUID = 1L;
	@Version
	@Column(name = "version")
	private int version;

	@Column(length = 15)
	private String phoneNumber;

	@Column
	private ListContactRole contactRole;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ContactPhone)) {
			return false;
		}
		ContactPhone other = (ContactPhone) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public ListContactRole getContactRole() {
		return contactRole;
	}

	public void setContactRole(ListContactRole contactRole) {
		this.contactRole = contactRole;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (phoneNumber != null && !phoneNumber.trim().isEmpty())
			result += "phoneNumber: " + phoneNumber;
		return result;
	}
}