

angular.module('taoDataSvc').controller('EditContactEmailController', function($scope, $routeParams, $location, flash, ContactEmailResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.contactEmail = new ContactEmailResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The contactEmail could not be found.'});
            $location.path("/ContactEmails");
        };
        ContactEmailResource.get({ContactEmailId:$routeParams.ContactEmailId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.contactEmail);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The contactEmail was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.contactEmail.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/ContactEmails");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The contactEmail was deleted.'});
            $location.path("/ContactEmails");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.contactEmail.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});