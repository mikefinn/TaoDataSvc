
angular.module('taoDataSvc').controller('NewTenantController', function ($scope, $location, locationParser, flash, TenantResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.tenant = $scope.tenant || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The tenant was created successfully.'});
            $location.path('/Tenants');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        TenantResource.save($scope.tenant, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Tenants");
    };
});