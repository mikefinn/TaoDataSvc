

angular.module('taoDataSvc').controller('EditListBusinessEntityTypeController', function($scope, $routeParams, $location, flash, ListBusinessEntityTypeResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.listBusinessEntityType = new ListBusinessEntityTypeResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listBusinessEntityType could not be found.'});
            $location.path("/ListBusinessEntityTypes");
        };
        ListBusinessEntityTypeResource.get({ListBusinessEntityTypeId:$routeParams.ListBusinessEntityTypeId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.listBusinessEntityType);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The listBusinessEntityType was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.listBusinessEntityType.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/ListBusinessEntityTypes");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listBusinessEntityType was deleted.'});
            $location.path("/ListBusinessEntityTypes");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.listBusinessEntityType.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});