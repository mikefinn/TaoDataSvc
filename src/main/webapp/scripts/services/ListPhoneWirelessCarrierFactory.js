angular.module('taoDataSvc').factory('ListPhoneWirelessCarrierResource', function($resource){
    var resource = $resource('rest/listphonewirelesscarriers/:ListPhoneWirelessCarrierId',{ListPhoneWirelessCarrierId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});