angular.module('taoDataSvc').factory('ListPersonRoleResource', function($resource){
    var resource = $resource('rest/listpersonroles/:ListPersonRoleId',{ListPersonRoleId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});