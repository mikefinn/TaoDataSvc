

angular.module('taoDataSvc').controller('EditListContactRoleController', function($scope, $routeParams, $location, flash, ListContactRoleResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.listContactRole = new ListContactRoleResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listContactRole could not be found.'});
            $location.path("/ListContactRoles");
        };
        ListContactRoleResource.get({ListContactRoleId:$routeParams.ListContactRoleId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.listContactRole);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The listContactRole was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.listContactRole.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/ListContactRoles");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listContactRole was deleted.'});
            $location.path("/ListContactRoles");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.listContactRole.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});