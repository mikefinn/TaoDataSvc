
angular.module('taoDataSvc').controller('NewLocationController', function ($scope, $location, locationParser, flash, LocationResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.location = $scope.location || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The location was created successfully.'});
            $location.path('/Locations');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        LocationResource.save($scope.location, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Locations");
    };
});