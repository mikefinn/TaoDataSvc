
angular.module('taoDataSvc').controller('NewPersonController', function ($scope, $location, locationParser, flash, PersonResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.person = $scope.person || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The person was created successfully.'});
            $location.path('/People');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        PersonResource.save($scope.person, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/People");
    };
});