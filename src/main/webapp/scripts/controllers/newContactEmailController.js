
angular.module('taoDataSvc').controller('NewContactEmailController', function ($scope, $location, locationParser, flash, ContactEmailResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.contactEmail = $scope.contactEmail || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The contactEmail was created successfully.'});
            $location.path('/ContactEmails');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        ContactEmailResource.save($scope.contactEmail, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/ContactEmails");
    };
});