

angular.module('taoDataSvc').controller('EditListPersonRoleController', function($scope, $routeParams, $location, flash, ListPersonRoleResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.listPersonRole = new ListPersonRoleResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listPersonRole could not be found.'});
            $location.path("/ListPersonRoles");
        };
        ListPersonRoleResource.get({ListPersonRoleId:$routeParams.ListPersonRoleId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.listPersonRole);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The listPersonRole was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.listPersonRole.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/ListPersonRoles");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listPersonRole was deleted.'});
            $location.path("/ListPersonRoles");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.listPersonRole.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});