

angular.module('taoDataSvc').controller('EditListPhoneWirelessCarrierController', function($scope, $routeParams, $location, flash, ListPhoneWirelessCarrierResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.listPhoneWirelessCarrier = new ListPhoneWirelessCarrierResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listPhoneWirelessCarrier could not be found.'});
            $location.path("/ListPhoneWirelessCarriers");
        };
        ListPhoneWirelessCarrierResource.get({ListPhoneWirelessCarrierId:$routeParams.ListPhoneWirelessCarrierId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.listPhoneWirelessCarrier);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The listPhoneWirelessCarrier was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.listPhoneWirelessCarrier.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/ListPhoneWirelessCarriers");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The listPhoneWirelessCarrier was deleted.'});
            $location.path("/ListPhoneWirelessCarriers");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.listPhoneWirelessCarrier.$remove(successCallback, errorCallback);
    };
    
    $scope.supportsSmsList = [
        "true",
        "false"
    ];
    $scope.isSMSEnabledList = [
        "true",
        "false"
    ];
    $scope.isMMSEnabledList = [
        "true",
        "false"
    ];
    
    $scope.get();
});