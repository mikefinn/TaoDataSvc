angular.module('taoDataSvc').factory('BusinessEntityResource', function($resource){
    var resource = $resource('rest/businessentities/:BusinessEntityId',{BusinessEntityId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});