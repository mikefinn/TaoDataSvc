

angular.module('taoDataSvc').controller('EditContactAddressController', function($scope, $routeParams, $location, flash, ContactAddressResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.contactAddress = new ContactAddressResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The contactAddress could not be found.'});
            $location.path("/ContactAddresses");
        };
        ContactAddressResource.get({ContactAddressId:$routeParams.ContactAddressId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.contactAddress);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The contactAddress was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.contactAddress.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/ContactAddresses");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The contactAddress was deleted.'});
            $location.path("/ContactAddresses");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.contactAddress.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});