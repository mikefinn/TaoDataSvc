angular.module('taoDataSvc').factory('ContactPhoneResource', function($resource){
    var resource = $resource('rest/contactphones/:ContactPhoneId',{ContactPhoneId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});