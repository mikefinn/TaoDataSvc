angular.module('taoDataSvc').factory('ContactEmailResource', function($resource){
    var resource = $resource('rest/contactemails/:ContactEmailId',{ContactEmailId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});