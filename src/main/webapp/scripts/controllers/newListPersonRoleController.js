
angular.module('taoDataSvc').controller('NewListPersonRoleController', function ($scope, $location, locationParser, flash, ListPersonRoleResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.listPersonRole = $scope.listPersonRole || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The listPersonRole was created successfully.'});
            $location.path('/ListPersonRoles');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        ListPersonRoleResource.save($scope.listPersonRole, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/ListPersonRoles");
    };
});