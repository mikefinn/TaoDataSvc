
angular.module('taoDataSvc').controller('NewBusinessEntityController', function ($scope, $location, locationParser, flash, BusinessEntityResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.businessEntity = $scope.businessEntity || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The businessEntity was created successfully.'});
            $location.path('/BusinessEntities');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        BusinessEntityResource.save($scope.businessEntity, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/BusinessEntities");
    };
});